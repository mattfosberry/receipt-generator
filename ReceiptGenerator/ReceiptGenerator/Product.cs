﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ReceiptGenerator
{
   public class IEntity
    {
       [Key]
        public String Name {get;set;}
    }

 
   [Table("Product")]
    public class Product : IEntity
    {

       [Required]
       public decimal Price { get; set; }
    }
   
}

