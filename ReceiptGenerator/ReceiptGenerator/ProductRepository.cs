﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReceiptGenerator
{
    public interface IRepository<T> where T : IEntity 
    {
        IEnumerable<T> List { get; }
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
        T FindByName(string Name);
    }

    public class ProductRepository :IRepository<Product>
    {
        ProductContext _productContext;

        public ProductRepository()
        {
            _productContext = new ProductContext();
        }


        public IEnumerable<Product> List 
        {
            get
            {
                return _productContext.Products;
            }
        }

        public void Add(Product entity)
        {
            _productContext.Products.Add(entity);
            _productContext.SaveChanges();
        }
        
        public void Delete(Product entity)
        {
            _productContext.Products.Remove(entity);
            _productContext.SaveChanges();
        }

        public void Update(Product entity)
        {
            _productContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            _productContext.SaveChanges();
        }

        public Product FindByName(string Name)
        {
            var result = (from r in _productContext.Products where r.Name == Name select r).FirstOrDefault();
            return result;
        }
   }
}
