﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReceiptGenerator
{
    class Program
    {
        static void Main(string[] args)
        {

            var o = new ProductRepository();

            Console.WriteLine("Enter a new product");
            var name = Console.ReadLine();
            Console.WriteLine("Enter price for product");
            var price = Console.ReadLine();

            var product = new Product();    
            product.Name = name;
            product.Price =  decimal.Parse(price);

            o.Add(product);

            foreach (var item in o.List)
            {
                Console.WriteLine(String.Format("{0} priced at £{1}", item.Name, item.Price.ToString("0.00")));
            }

          
        }
    }
}
